package u02

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Lab2.Shape.{Circle, Rectangle, Square, area, perimeter}
import u02.Lab2._
import u02.Optionals._
import u02.Optionals.Option._

class Lab2Test {

  val empty : String => Boolean = _ == ""

  @Test def test3A(){
    assertEquals("ODD", parityMethod(5))
    assertEquals("EVEN",parityMethod(6))
    assertEquals("ODD", parityLiteral(5))
    assertEquals("EVEN",parityLiteral(6))

  }

  @Test def test3B(){
    val notEmptyLiteral = negLiteral(empty)
    val notEmptyMethod = negMethod(empty)
    assertTrue(notEmptyLiteral("foo"))
    assertTrue(notEmptyMethod("foo"))
    assertFalse(notEmptyLiteral(""))
    assertFalse(notEmptyMethod(""))
    assertTrue(notEmptyLiteral("foo") && !notEmptyLiteral(""))
    assertTrue(notEmptyMethod("foo") && !notEmptyMethod(""))
  }
  @Test def test3C(){
    val genMethod = negGenericMethod[Int](num => num < 10)
    assertFalse(genMethod(0))
    assertTrue(genMethod(10))
    assertTrue(!genMethod(0) && genMethod(10))
  }

  @Test def test4(){
    assertTrue(p1(1)(2)(3))
    assertTrue(p2(1,2,3))
    assertTrue(p3(1)(2)(3))
    assertTrue(p4(1,2,3))

    assertFalse(p1(3)(2)(1))
    assertFalse(p2(3,2,1))
    assertFalse(p3(3)(2)(1))
    assertFalse(p4(3,2,1))
  }

  @Test def test5(){
    assertEquals(9,composeMethod[Int,Int,Int](_-1,_*2)(5))
  }
  @Test def test6(){
    assertEquals(0,fib(0))
    assertEquals(1,fib(1))
    assertEquals(1,fib(2))
    assertEquals(2,fib(3))
    assertEquals(3,fib(4))
  }
  @Test def test7(){
    assertEquals(18,perimeter(Rectangle(5,4)))
    assertEquals( 31.41592653589793,perimeter(Circle(5)))
    assertEquals(20,perimeter(Square(5)))

    assertEquals(20,area(Rectangle(5,4)))
    assertEquals(78.53981633974483,area(Circle(5)))
    assertEquals(25,area(Square(5)))
  }
  @Test def test8(){
    val s1: Option[Int] = Some(1)
    val s2: Option[Int] = Some(2)
    val none: Option[Int] = None()

    assertEquals(none,filter(none)(_>2))
    assertEquals(none,filter(s1)(_ >= 2))
    assertEquals(s2,filter(s2)(_ >= 2))

    assertEquals(Some(true),map(s2)(_ >= 2))
    assertEquals(Some(false),map(s2)(_ >= 20))
    assertEquals(none,map(none)(_>2))

    assertEquals(none,map2(none,s1))
    assertEquals(none,map2(s1,none))
    assertEquals(Pair(1,"ab"),map2(s1,Some("ab")))
  }


}