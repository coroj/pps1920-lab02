package u02

object Lab2{//extends App{
  //3a
  def parityMethod(n : Int): String = n%2 match {
    case 1 => "ODD"
    case _ => "EVEN"
  }

  val parityLiteral: Int => String = n => n%2 match{
    case 1 => "ODD"
    case _ => "EVEN"
  }

  //3b

  val negLiteral: (String => Boolean) => String => Boolean =
    f => s => !f(s)

  def negMethod(predicate: String => Boolean) : String => Boolean =
    s => !predicate(s)

  //3c

  def negGenericMethod[A](predicate: A => Boolean) : A => Boolean =
    s => !predicate(s)


  //4
  val p1: Int => Int => Int => Boolean = x => y => z => x<=y && y<=z
  val p2: (Int,Int,Int) => Boolean = (x,y,z) => x<=y && y<=z

  def p3(x:Int)(y:Int)(z:Int) : Boolean = x<=y && y<=z
  def p4(x:Int, y:Int, z:Int) : Boolean = x<=y && y<=z

  //5

  def composeMethod[A,B,C](f: B => C, g: A => B) : A => C =
    value => f(g(value))

  //6

  def fib(n : Int) : Int = n match{
      case 0 => 0
      case 1 => 1
      case n => fib(n-1) + fib(n-2)
    }

  //7
  sealed trait Shape
  object Shape {

    case class Rectangle(b: Double, h: Double) extends Shape

    case class Circle(r: Double) extends Shape

    case class Square(l: Double) extends Shape

    def perimeter(shape: Shape) : Double = shape match{
      case Rectangle(b,h) => 2*b + 2*h
      case Circle(r) => 2*Math.PI*r
      case Square(l) => l*4
    }

    def area(shape: Shape) : Double = shape match{
      case Rectangle(b,h) => b*h
      case Circle(r) => Math.PI*r*r
      case Square(l) => l*l
    }
  }

  //8 -> in Optionals

  //challenge -> in BTrees.scala









}
