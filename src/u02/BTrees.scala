package u02

object BTrees extends App {

  // A custom and generic binary tree of elements of type A
  sealed trait Tree[A]
  object Tree {
    case class Leaf[A](value: A) extends Tree[A]
    case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

    def traversing[A,B](t: Tree[A], operand: (B,B) => B,leaf: A => B) : B = t match{
      case Branch(l,r) => operand(traversing(l,operand,leaf),traversing(r,operand,leaf))
      case Leaf(e) => leaf(e)
    }
    def size[A](tree:Tree[A]) : Int =  traversing[A,Int](tree,(l,r) => l+r , _ => 1)
    def find[A](tree:Tree[A], value:A) : Boolean = traversing[A,Boolean](tree,(l,r) => l||r ,e => e == value)
    def count[A](tree:Tree[A],value:A) : Int = traversing[A,Int](tree,(l,r) => l+r ,e => if(e == value) 1 else 0)
  }

}
